const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const Usuario = require('../mvc/models/usuario');

passport.use( new LocalStrategy( {usernameField:'email', passwordField:'password'},
    async (email, password, done) => {   
        try {
            var usuario = await Usuario.findOne( {email} );

            if (!usuario) {
                return done(null, false, {message:'Email no existente o incorrecto.'});
            }
    
            //Si se encuentra al usuario
            if(  !usuario.validPassword(password) ) {                
                return done(null, false, {message:'Password inválido.'});
            }

            return done(null, usuario);
        
        } catch(error) {
            console.log(error);
            return done(error);
        }        
    }
));

passport.serializeUser( function(user, callback) {
    callback(null, user.id );
} )


passport.deserializeUser( function(id, callback) {
    Usuario.findById(id, function(err, usuario) {
        callback(err, usuario);
    });
});


module.exports = passport;