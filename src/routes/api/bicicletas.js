var express = require('express');
var router = express.Router();

var bicicletaController = require('../../mvc/controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list );
router.post('/create', bicicletaController.bicicleta_create );

router.post('/:code/update', bicicletaController.bicicleta_update );
router.put('/:code/update', bicicletaController.bicicleta_update );

/* Post y delete para las pruebas */
router.post('/:code/delete', bicicletaController.bicicleta_delete );
router.delete('/:code/delete', bicicletaController.bicicleta_delete );

module.exports = router;