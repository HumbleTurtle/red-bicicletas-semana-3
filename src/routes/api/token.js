var express = require('express');
var router = express.Router();

var authControllerAPI = require('../../mvc/controllers/api/authControllerAPI');

router.post('/forgotPassword', authControllerAPI.forgotPassword );
router.post('/authenticate', authControllerAPI.authenticate );


module.exports = router;