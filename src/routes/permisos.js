
function loggedIn(req, res, next) {
    if(req.user) {
        next();
    } else {
        console.log('User sin logearse');
        res.redirect('/auth/login');
    }
}

module.exports = {
    loggedIn
}