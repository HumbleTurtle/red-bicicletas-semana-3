var express = require('express');
var path = require('path');

// Subrutas, rutas relativas
var routesAPI = require('./routesAPI');
var routesLocal = require('./routesLocal');

exports.register = function(app) {
    // Ruta estatica
    app.use(express.static(path.join('./src','static')));

    // Registramos el router del API
    app.use('/api', routesAPI );

    // Registramos el router local
    app.use('/', routesLocal );
}