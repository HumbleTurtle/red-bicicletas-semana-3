const express = require('express');
const path = require('path');

const router = express.Router();
const jwt = require('jsonwebtoken');


// Subrutas
var bicicletasAPIRouter = require('./api/bicicletas');
var usuariosAPIRouter = require('./api/usuarios');
var tokenAPIRouter = require('./api/token');

router.use("/bicicletas",validarUsuario, bicicletasAPIRouter );
router.use("/usuarios", usuariosAPIRouter );
router.use("/auth", tokenAPIRouter );


async function validarUsuario(req, res, next) {
    try{
        var decoded = await jwt.verify(req.headers['x-access-token'], req.app.get('secretKey') );
        
        req.body.userId = decoded.id;
        
        console.log('jwt verify: '+ decoded);

        next();
    
    } catch(err) {
        console.log(err);
        res.json({status:'error', message: err.messsage, data: null});        
    }
}

module.exports = router;